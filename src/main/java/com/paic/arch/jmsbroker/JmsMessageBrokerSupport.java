package com.paic.arch.jmsbroker;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

import com.paic.arch.jmsbroker.achieve.JmsAchieve;
import com.paic.arch.jmsbroker.factory.JmsFactory;
import com.paic.arch.jmsbroker.factory.JmsType;

/**
 * JMS中间件工具
 * @author liu_shuangyuan
 * @date   20180301
 * @version V1.0
 */
public class JmsMessageBrokerSupport {

    /**
     * JMS实现，默认ActiveMQ
     */
    private static JmsType DEFULT_JMS_TYPE = JmsType.ACTIVEMQ;

    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private String brokerUrl;

    private JmsAchieve achieve;

    private JmsMessageBrokerSupport(String aBrokerUrl) {
        this.brokerUrl = aBrokerUrl;
        // 工厂创建对应实现类
        achieve = JmsFactory.getAchieve(DEFULT_JMS_TYPE);
        achieve.setUrl(aBrokerUrl);
    }
    
    /**
     * 设置JMS实现
     * @param type
     */
    public static void setJmsType(JmsType type){
        DEFULT_JMS_TYPE = type;
    }

    /**
     * 获取一个新实例
     * @param aBrokerUrl
     * @return
     */
    public static JmsMessageBrokerSupport getNewInstance(String aBrokerUrl) {
        return new JmsMessageBrokerSupport(aBrokerUrl);
    }
    

    public final JmsMessageBrokerSupport andThen() {
        return this;
    }

    /*******************************************************************
     * Support method                                                  
     *******************************************************************/
    /**
     * 创建一个新Support,URL自动获取
     * @return
     * @throws Exception
     */
    public static JmsMessageBrokerSupport createJmsSupport() throws Exception {
        return createJmsSupport(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    }

    /**
     * 创建一个新Support
     * @return
     * @throws Exception
     */
    public static JmsMessageBrokerSupport createJmsSupport(String url) throws Exception {
        JmsMessageBrokerSupport support = new JmsMessageBrokerSupport(url);
        support.initSupport().startSupport();
        return support;
    }

    /**
     * 停止当前Support
     * @return
     * @throws Exception
     */
    public JmsMessageBrokerSupport stopJmsSupport() throws Exception {
        achieve.stopSupport();
        return this;
    }

    public long getMsgCountAt(String aDestinationName) throws Exception {
        return achieve.getMsgCountAt(aDestinationName);
    }

    /*******************************************************************
     * Customer method                                                  
     *******************************************************************/

    public String getASingleMsgFromDestination(String aDestinationName) {
        return getASingleMsgFromDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    public String getASingleMsgFromDestination(String aDestinationName, final int aTimeout) {
        return achieve.getASingleMsgFromDestination(aDestinationName, aTimeout);
    }

    public final String getBrokerUrl() {
        return brokerUrl;
    }

    /*******************************************************************
     * Producer method                                                  
     *******************************************************************/

    public JmsMessageBrokerSupport sendATextMsgToDestinationAt(String aDestinationName, final String aMessageToSend) {
        achieve.sendATextMsgToDestinationAt(aDestinationName, aMessageToSend);
        return this;
    }

    /*******************************************************************
     * private method                                                  
     *******************************************************************/

    private JmsMessageBrokerSupport initSupport() throws Exception {
        achieve.setUrl(brokerUrl).initSupport();
        return this;
    }

    private JmsMessageBrokerSupport startSupport() throws Exception {
        achieve.startSupport();
        return this;
    }

    /*******************************************************************
     * Deprecated method or innerClass
     *******************************************************************/

    @Deprecated
    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createJmsSupport();
    }

    @Deprecated
    public void stopTheRunningBroker() throws Exception {
        stopJmsSupport();
    }

    @Deprecated
    public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        return getNewInstance(aBrokerUrl);
    }

    @Deprecated
    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName,
            final String aMessageToSend) {
        return sendATextMsgToDestinationAt(aDestinationName, aMessageToSend);
    }

    @Deprecated
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return getMsgCountAt(aDestinationName);
    }

    @Deprecated
    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    @Deprecated
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        try {
            return getASingleMsgFromDestination(aDestinationName, aTimeout);
        } catch (com.paic.arch.jmsbroker.exception.NoMessageReceivedException e) {
            throw new NoMessageReceivedException(e.getMessage());
        }
    }

    @Deprecated
    public static class NoMessageReceivedException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public NoMessageReceivedException(String reason) {
            super(reason);
        }
    }

}
