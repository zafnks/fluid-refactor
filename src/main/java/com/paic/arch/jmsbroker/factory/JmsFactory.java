package com.paic.arch.jmsbroker.factory;

import com.paic.arch.jmsbroker.achieve.JmsAchieve;
import com.paic.arch.jmsbroker.achieve.activemq.ActiveMqAchieve;
import com.paic.arch.jmsbroker.achieve.zeromq.ZeroMqAchieve;

/**
 * 消息实现工厂
 * @author liu_shuangyuan
 * @date   20180301
 * @version V1.0
 */
public class JmsFactory {

    public static JmsAchieve getAchieve(JmsType type) {
        JmsAchieve achieve = null;
        switch (type) {
        case ACTIVEMQ:
            achieve = new ActiveMqAchieve();
            break;
        case ZEROMQ:
            achieve = new ZeroMqAchieve();
            break;
        default:
            achieve = new ActiveMqAchieve();
            break;
        }
        return achieve;
    }

}
