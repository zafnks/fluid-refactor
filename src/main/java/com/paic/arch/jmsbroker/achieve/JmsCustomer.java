package com.paic.arch.jmsbroker.achieve;

public interface JmsCustomer {
    
    public long getMsgCountAt(String aDestinationName) throws Exception;
    
    public String getASingleMsgFromDestination(String aDestinationName, final int aTimeout);

}
