package com.paic.arch.jmsbroker.achieve;

public interface JmsProducer {
    
    public JmsProducer sendATextMsgToDestinationAt(String aDestinationName, final String aMessageToSend);
    

}
