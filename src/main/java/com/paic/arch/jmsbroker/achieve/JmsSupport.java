package com.paic.arch.jmsbroker.achieve;

public interface JmsSupport {
    
    public JmsSupport setUrl(String url);
        
    public JmsSupport initSupport() throws Exception;
    
    public JmsSupport startSupport() throws Exception;
    
    public JmsSupport stopSupport() throws Exception;

}
