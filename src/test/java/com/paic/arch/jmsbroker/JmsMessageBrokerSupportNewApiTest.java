package com.paic.arch.jmsbroker;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.paic.arch.jmsbroker.exception.NoMessageReceivedException;
import com.paic.arch.jmsbroker.factory.JmsType;

/**
 * 使用新API测试
 * @author 刘双源
 * @date   20180301
 * @version V1.0
 */
public class JmsMessageBrokerSupportNewApiTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static JmsMessageBrokerSupport JMS_SUPPORT;
    private static String REMOTE_BROKER_URL;

    @BeforeClass
    public static void setup() throws Exception {
        // 选择JMS实现
        JmsMessageBrokerSupport.setJmsType(JmsType.ACTIVEMQ);
        JMS_SUPPORT = JmsMessageBrokerSupport.createJmsSupport();
        REMOTE_BROKER_URL = JMS_SUPPORT.getBrokerUrl();
    }

    @AfterClass
    public static void teardown() throws Exception {
        JMS_SUPPORT.stopJmsSupport();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        JmsMessageBrokerSupport.getNewInstance(REMOTE_BROKER_URL)
                .andThen().sendATextMsgToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
        long messageCount = JMS_SUPPORT.getMsgCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = JmsMessageBrokerSupport.getNewInstance(REMOTE_BROKER_URL)
                .sendATextMsgToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().getASingleMsgFromDestination(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }

    @Test(expected = NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        JmsMessageBrokerSupport.getNewInstance(REMOTE_BROKER_URL).getASingleMsgFromDestination(TEST_QUEUE, 1);
    }


}